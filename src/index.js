const fs = require('fs');
const Discord = require('discord.js');
const { prefix, token } = require('./config.json');

const client = new Discord.Client();
client.commands = new Discord.Collection();

const commandFiles = fs
	.readdirSync('./commands')
	.filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
	const command = require(`./commands/${file}`);
	client.commands.set(command.name, command);
}

client.once('ready', () => {
	console.log('Ready!');
});

client.on('message', async message => {
	if (!message.content.startsWith(prefix) || message.author.bot) return;

	const args = message.content.slice(prefix.length).split(/ +/);
	const commandName = args.shift().toLowerCase();
	const command = client.commands.get(commandName);

	if (command.args && !args.length) {
		let reply = `Você não indicou nenhum argumento, ${message.author}!`;

		if (command.usage) {
			reply += `\nO jeito certo de usar é \`${prefix}${command.name} ${
				command.usage
			}\``;
		}

		return message.channel.send(reply);
	}

	if (!client.commands.has(commandName)) return;

	try {
		command.execute(message, args);
	}
	catch (err) {
		console.log(err);
		message.reply('Houve um erro ao executar este comando.');
	}
});

client.login(token);
