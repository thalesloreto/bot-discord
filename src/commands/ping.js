module.exports = {
	name: 'ping',
	description: 'Ping!',
	async execute(message) {
		const m = await message.channel.send('Ping?');
		await message.channel.send(
			`Pong! Latência do server: ${m.createdTimestamp -
				message.createdTimestamp}ms`
		);
	},
};
