module.exports = {
	name: 'avatar',
	description: 'list member\'s avatar',
	async execute(message) {
		if (!message.mentions.users.size) {
			return await message.channel.send(
				`${message.author} avatar: ${message.author.displayAvatarURL}`
			);
		}

		const avatarList = await message.mentions.users.map(user => {
			return `${user} avatar: ${user.displayAvatarURL}`;
		});
		message.channel.send(avatarList);
	},
};
