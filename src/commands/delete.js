module.exports = {
	name: 'delete',
	description: 'use for delete any rows in your chat',
	args: true,
	usage: '<1-99>',

	async execute(message, args) {
		const amount = parseInt(args[0]) + 1;

		message.channel.bulkDelete(amount, true).catch(err => {
			console.error(err);
			message.channel.send(
				'OPS !@# Ocorreu um erro ao apagar as mensagens deste canal.'
			);
		});
	},
};
