module.exports = {
	name: 'args-test',
	description: 'args test by param',
	args: true,

	async execute(message, args) {
		if (args[0] === 'foo') {
			return await message.channel.send('faaa');
		}

		await message.channel.send(`Argumentos: ${args}\nPrimeiro arg: ${args[0]}`);
	},
};
