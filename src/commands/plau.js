const ytdl = require('ytdl-core');

module.exports = {
	name: 'plau',
	description: 'play a music',
	usage: '<music name>',

	execute(message) {
		if (message.channel.type !== 'text') return;

		const { voiceChannel } = message.member;

		if (!voiceChannel) {
			return message.reply('Você tem que estar em uma sala de voz!');
		}

		voiceChannel.join().then(connection => {
			const stream = ytdl(
				'https://www.youtube.com/watch?v=uuVjaPfjpSY&feature=youtu.be',
				{
					filter: 'audioonly',
				}
			);
			const dispatcher = connection.playStream(stream);

			dispatcher.on('end', () => voiceChannel.leave());
		});
	},
};
