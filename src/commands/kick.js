module.exports = {
	name: 'kick',
	description: 'kick any player',
	async execute(message) {
		const taggedUser = await message.mentions.users.first();

		if (!message.mentions.users.size) {
			return await message.reply('você tem que mencionar alguém.');
		}

		await message.channel.send(
			`Você solicitou kickar o -> ${taggedUser.username}`
		);
	},
};
