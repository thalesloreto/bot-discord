module.exports = {
	name: 'server',
	description: 'description of server',
	async execute(message) {
		await message.channel.send(
			`Servidor: ${message.guild.name}\nMembros: ${
				message.guild.memberCount
			}\nCriador: ${message.guild.owner}\nCriado em: ${message.guild.createdAt}`
		);
	},
};
